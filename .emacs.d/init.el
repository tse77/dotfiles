;;; package --- Summary
;;; commentary:
;;; Accomodate for non-standard path to init.el

;;; Added by Package.el.  This must come before configurations of
;;; installed packages.  Don't delete this line.  If you don't want it,
;;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

;;; Misc settings
;;; #############
(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

;; ;;;;Package stuff
;; (require 'package)

;; ;;Add melpa repo (third party repo, bigger selection)
;; (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

;; ;;Add org elpa repo (to have newer Org mode versions)
;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))

;; (package-initialize)

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

;;Add org elpa repo (to have newer Org mode versions)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(package-initialize)

;;Make sure use-package is installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Manually installed .el files
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; UTF-8 as default encoding
(set-language-environment "UTF-8")

;; Enable column number
(setq column-number-mode t)

(setq indent-tabs-mode t)
(setq tab-width 4)
;; Make wrod wrap default
(global-visual-line-mode t)

;; Save/load desktop sessions automatically
(desktop-save-mode 1)

;;
(setq confirm-kill-emacs 'y-or-n-p)

;; ediff customization
(setq ediff-split-window-function (quote split-window-horizontally))
(setq ediff-window-setup-function (quote ediff-setup-windows-plain))


;; Automatically download missing packages
(setq use-package-always-ensure t)

;; To speed up tramp mode, faster than default scp
(setq tramp-default-method "ssh")

;; Store backup files in temp folder
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(setq show-paren-delay 0)
(show-paren-mode t)
;; (setq show-paren-style 'expression)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time

;; Project managment.
;; "Project" means GIT repo most of the time for us.
;; If you want to mark a directory manually as a project just create an
;; empty file named ".projectile" in that directory
(use-package projectile
  :demand t
  :config
  (projectile-mode)
  ;; (setq projectile-keymap-prefix (kbd "C-c p"))
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

(use-package go-projectile
  :config
  (require 'go-projectile))

(use-package ibuffer-projectile)

(use-package move-text
  :demand t
  :config
  (require 'move-text)
  (move-text-default-bindings))

;; (use-package emacsql-sqlite
;; 	:ensure t)

(use-package emacsql-sqlite3
  :ensure t)

(use-package direnv
 :config
 (direnv-mode))

(use-package ibuffer-vc
  :demand t
  :config
  (add-hook 'ibuffer-hook
	    (lambda ()
	      (ibuffer-vc-set-filter-groups-by-vc-root)
	      (unless (eq ibuffer-sorting-mode 'alphabetic)
		(ibuffer-do-sort-by-alphabetic))))
  (global-set-key (kbd "C-x C-b") 'ibuffer))

;; Press the first key in a key combo and wait a while to get a list
;; of all possible keys currently available to you.
(use-package which-key
  :demand t
  :config
  (which-key-mode))

;; Handle multiple workspaces within emacs
(use-package eyebrowse
  :diminish eyebrowse-mode
  :config (progn
            (define-key eyebrowse-mode-map (kbd "M-1") 'eyebrowse-switch-to-window-config-1)
            (define-key eyebrowse-mode-map (kbd "M-2") 'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "M-3") 'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "M-4") 'eyebrowse-switch-to-window-config-4)
	    (define-key eyebrowse-mode-map (kbd "M-5") 'eyebrowse-switch-to-window-config-5)
	    (define-key eyebrowse-mode-map (kbd "M-6") 'eyebrowse-switch-to-window-config-6)
	    (define-key eyebrowse-mode-map (kbd "M-7") 'eyebrowse-switch-to-window-config-7)
	    (define-key eyebrowse-mode-map (kbd "M-8") 'eyebrowse-switch-to-window-config-8)
	    (define-key eyebrowse-mode-map (kbd "M-9") 'eyebrowse-switch-to-window-config-9)
	    ;; (eyebrowse-setup-opinionated-keys)
            (eyebrowse-mode t)
            (setq eyebrowse-new-workspace t)))

;; Snippets with yasnippets
(use-package yasnippet
  :config
  (yas-reload-all)
  (add-hook 'prog-mode-hook #'yas-minor-mode))

(use-package yasnippet-snippets)

;; GIT interface, THE GIT interface
(use-package magit
  :demand hej ; Anything other than nil and () is considered True in Lisp
  :config
  (global-set-key (kbd "C-x g") 'magit-status)
  (global-set-key (kbd "C-x M-l") 'magit-log-buffer-file))

;; Completion framework
(use-package ivy
  :demand victory
  :config
  (ivy-mode 1)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t))

(use-package ivy-rich
  :after ivy
  :ensure t
  :config
  (ivy-rich-mode 1)
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))


;; A bunch of commands that uses ivy
(use-package counsel
  :demand victory
  :bind (("M-Y" . counsel-yank-pop)
	 ("C-x C-f" . counsel-find-file)
	 ("C-h f" . counsel-describe-function)
	 ("C-h v" . counsel-describe-variable)
	 ("M-x" . counsel-M-x)))

(use-package counsel-projectile
  :config
  (counsel-projectile-mode))

;; Search buffer(s) using ivy
(use-package swiper
  :demand victory
  :config
  (global-set-key "\C-s" 'swiper))

;; wgrep
(use-package wgrep
  :ensure t)

;; Multiple cursors support
(use-package multiple-cursors
  :demand true
  :bind (("C-S-c C-S-c" . mc/edit-lines)
		 ("C->" . mc/mark-next-like-this)
		 ("C-<" . mc/mark-next-like-this)))

;; Use Shift+<arrow-keys> to move between windows
(use-package windmove
  :config
  (windmove-default-keybindings)
  (setq windmove-wrap-around t))

(global-set-key [C-tab] 'other-window)
(global-set-key (kbd "C-S-<iso-lefttab>") '(lambda ()
					     (interactive)
					     (other-window -1)))

;; Jump to/delete/swap arbitrary windows
(use-package ace-window
  :demand t
  :bind ("M-B" . ace-window)
  :config
  (defvar aw-dispatch-alist
    '((?x aw-delete-window " Ace - Delete Window")
      (?m aw-swap-window " Ace - Swap Window")
      (?n aw-flip-window)
      (?v aw-split-window-vert " Ace - Split Vert Window")
      (?b aw-split-window-horz " Ace - Split Horz Window")
      (?i delete-other-windows " Ace - Maximize Window")
      (?o delete-other-windows))
    "List of actions for `aw-dispatch-default'."))

;; Toggle "fullscreen" mode on/off when we have multiple windows
;; (use-package zygospore
;;   :demand t
;;   :bind* ("M-1" . zygospore-toggle-delete-other-windows))

;; Complete anything
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))

;; company frontend
(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package dumb-jump
  :bind (("C-M-." . dumb-jump-go))
  :config (setq dumb-jump-selector 'ivy))

;; YAML
(use-package yaml-mode)

;; protobuf
(use-package protobuf-mode)

;;; Flycheck-related
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode))

(use-package flycheck-golangci-lint
  :ensure t
  :hook
  (go-mode . flycheck-golangci-lint-setup)
  :config
  ;; Disable so that we can run golangci-lint through flycheck as default
  ;; This have to be set prior lsp-mode to work
  ;; See this issue: https://github.com/emacs-lsp/lsp-mode/issues/1413
  (setq lsp-diagnostic-package :none)
  (setq flycheck-checker 'golangci-lint))
;; Checker chains does not seems to work at the moment
;; See this issue: https://github.com/flycheck/flycheck/issues/1762
  ;;(flycheck-add-next-checker 'golangci-lint 'lsp))

;; xclipboard integration
(use-package xclip
  :demand t
  :config
  (xclip-mode 1))

;; A Collection of Ridiculously Useful eXtensions for Emacs
(use-package crux
  :demand t
  :bind (("C-c I" . crux-find-user-init-file)))

(use-package exec-path-from-shell
  :demand t
  :config
  (setq exec-path-from-shell-check-startup-files nil)
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

(use-package treemacs
  :ensure t
  :bind
  ("C-c t" . treemacs)
  ("C-c C-t" . treemacs-select-window)
  :init

  (use-package treemacs-icons-dired
    :ensure t)
  (use-package treemacs-magit
    :ensure t)
  (use-package treemacs-projectile
    :ensure t))

;;; Language packages
(use-package go-snippets)

;;; LSP
(use-package lsp-mode
  :hook ((prog-mode . lsp-deferred)
	 (lsp-mode . lsp-enable-which-key-integration))
  :commands (lsp lsp-deferred)
  :config
  ;; uncomment below line for debugging pyls
  ;; (setq lsp-pyls-server-command '("pyls" "-v"  "--log-file" "/tmp/pyls.log"))
  ;; Add Golang projects folders to ignore
  (dolist (dir '(
                 "[/\\\\]vendor$"
				 "[/\\\\].go$"))
    (push dir lsp-file-watch-ignored-directories))
  (setq
   lsp-idle-delay 0.5
   ;; Disable code formatting through lsp since we want to run black formatting
   ;; manual (black does not integrate with Python2 pyls )
   lsp-pyls-plugins-autopep8-enabled nil
   lsp-pyls-plugins-yapf-enabled nil
   ;; Enable lsp-pyls features
   lsp-pyls-plugins-pylint-enabled t
   ;; https://emacs-lsp.github.io/lsp-mode/page/performance/
   gc-cons-threshold 100000000
   read-process-output-max (* 1024 1024) ;; 1mb
   )
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "s-p")
  ;; Language mode hooks
  (add-hook 'go-mode-hook
	    (lambda ()
	      (setq indent-tabs-mode 1)
	      (setq tab-width 4)))
  (add-hook 'python-mode-hook
	    (lambda ()
	      (setq lsp-before-save-edits nil
		    python-shell-interpreter "python2"))))

;; lsp extras
(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq
   lsp-ui-doc-show-with-cursor t
   ))

(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list)

(use-package lsp-ivy
  :commands lsp-ivy-workspace-symbol)

;; (use-package dap-mode
;; 	:ensure t
;; 	:init
;; 	(require 'dap-go))

(use-package lsp-docker
  :config
  ;; Functions to be able to use different container images per Python project
  (defun my/guess-lsp-docker-project ()
    (if-let ((buffer-file buffer-file-name)
	     (dir (f--traverse-upwards (f-exists? (f-expand ".lsp-docker" it)) (f-dirname buffer-file)))
	     (file (f-join dir ".lsp-docker"))
	     (image (with-temp-buffer
		      (insert-file-contents file)
		      (s-chomp
		       (buffer-substring-no-properties (point-min) (point-max)))))
	     (n (length image)))
	`(:root ,dir :image ,image)))

  (cl-defun my/register-python-client (&key root image)
    (lsp-docker-register-client
     :server-id 'pyls
     :priority 2
     :docker-server-id 'pyls-docker
     :docker-image-id image
     :docker-container-name (format "pyls-%s" (substring (secure-hash 'md5 root) 0 7))
     :server-command "pyls"
     :path-mappings `((,root . "/work"))
     :launch-server-cmd-fn #'lsp-docker-launch-new-container))

  (defun my/enable-lsp-docker ()
    ;; (require 'lsp-clients)
    (if-let ((project (my/guess-lsp-docker-project)))
	(pcase major-mode
	  ('python-mode (apply #'my/register-python-client project)))))

  (with-eval-after-load 'python
	(add-hook 'python-mode-hook #'my/enable-lsp-docker)))

;; Kubernetes stuff
(use-package kubernetes
  :ensure t
  :commands (kubernetes-overview))

;; Extra Python related packages until lsp-mode/palantir format works on large files as well
;; Sort Python imports
(use-package py-isort
  :ensure t)
;;  :init
;;  (add-hook 'python-mode-hook 'py-isort-before-save))

;; (use-package py-yapf
;;   :ensure t
;;   :init)
;;  (add-hook 'python-mode-hook 'py-yapf-enable-on-save))

(use-package blacken
  :ensure t
  :init
  (setq blacken-line-length 80))
;;  (add-hook 'python-mode-hook 'blacken-mode))

;; Emacs looks
;; Fonts
;; (set-frame-font "Hack-11" nil t)
(set-frame-font "inconsolata-13" nil t)
(use-package all-the-icons
  :ensure t)

(use-package fontawesome
  :ensure t)

(use-package modus-themes
  :ensure
  :init
  ;; Add all your customizations prior to loading the themes
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs nil
        modus-themes-region '(bg-only no-extend))

  ;; Load the theme files before enabling a theme
  (modus-themes-load-themes)
  :config
  ;; Load the theme of your choice:
  ;; (modus-themes-load-operandi) ;; OR (modus-themes-load-vivendi)
  (modus-themes-load-vivendi)
  :bind ("<f5>" . modus-themes-toggle))

;; Google material theme
;; (use-package material-theme
;;   :ensure t
;;   :config
;;   (load-theme 'material t)
;;   (global-set-key (kbd "C-c s") 'toggle-dark-light)
;;   (setq theme-which-enable  'dark))

;; Dark o light func
;; (defvar theme-which-enable nil
;; "Variable indicate which theme (dark or light) is being used.")

;; (defun toggle-dark-light ()
;;   (interactive)
;;   (if (eq theme-which-enable 'dark)
;; 	  (progn (load-theme 'modus-vivendi t)
;; 		 (setq theme-which-enable 'light))
;;     (progn (load-theme 'modus-operandi t)
;; 	   (setq theme-which-enable  'dark))))

;; (use-package doom-themes
;;   :config
;;   ;; (load-theme 'doom-vibrant t)
;;   (setq theme-which-enable  'dark)
;;   ;; (global-set-key (kbd "C-c s") 'toggle-dark-light)
;;   (setq doom-one-brighter-comments t)
;;   (setq doom-one-comment-bg t)
;;   (setq doom-vibrant-brighter-comments t)
;;   (setq doom-vibrant-brighter-modeline t)
;;   (setq doom-vibrant-comment-bg t))

(use-package doom-modeline
  :ensure t
  :hook
  (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-buffer-file-name-style 'buffer-name)
  (setq doom-modeline-bar-width 3)
  (setq doom-modeline-height 25)
  (setq doom-modeline-minor-modes nil)
  (setq doom-themes-enable-bold nil))

;; (use-package smart-mode-line
;;   :ensure t
;;   :config
;;   (setq sml/theme 'dark))

;; Disable toolbar
(tool-bar-mode -1)

;; Disable scrollbar
(scroll-bar-mode -1)

;; Refresh files to buffer automatically
(global-auto-revert-mode 1)
(put 'dired-find-alternate-file 'disabled nil)

;; org-mode

(use-package org
  :ensure t
  :config
  (progn
;; This is for org-mode agenda view activation
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
;; This is for key bindings to invoke agenda mode (see line-2)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
(global-set-key (kbd "<f9>") (lambda() (interactive)(find-file "~/Dropbox/gtd/gtd.org")))
(global-set-key (kbd "<f10>") (lambda() (interactive)(find-file "~/Dropbox/gtd/inbox.org")))
(global-set-key (kbd "<f11>") (lambda() (interactive)(find-file "~/Dropbox/gtd/someday.org")))
;; (global-set-key (kbd "<f12>") (lambda() (interactive)(find-file "~/Dropbox/gtd/tickler.org")))

(setq org-link-frame-setup '((file . find-file)))
(setq org-hide-leading-stars t)

(setq org-agenda-files '("~/Dropbox/gtd/inbox.org"
                         "~/Dropbox/gtd/gtd.org"
                         "~/Dropbox/gtd/tickler.org"))
(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/Dropbox/gtd/inbox.org" "Tasks")
                               "* TODO %i%?")
                              ("T" "Tickler" entry
                               (file+headline "~/Dropbox/gtd/tickler.org" "Tickler")
                               "* %i%? \n %U")))
(setq org-refile-targets '(("~/Dropbox/gtd/gtd.org" :maxlevel . 3)
                           ("~/Dropbox/gtd/someday.org" :level . 1)
                           ("~/Dropbox/gtd/tickler.org" :maxlevel . 2)))
(setq org-todo-keywords '((sequence "TODO(t)" "DOING(g)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)"))))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode)))
;; End org-mode

;; Deft mode
(use-package deft
  :after org
  :bind
  ("C-c n d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (setq deft-extensions '("org" "txt" "tex"))
  (deft-directory "~/Dropbox/deft"))
;; End Deft mode

;; org-roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/Dropbox/deft/roam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol)
  (setq org-roam-v2-ack t)
  (setq org-roam-completion-everywhere t))
;; End org-roam

;; Execution paths
(add-to-list 'exec-path "~/bin")
(add-to-list 'exec-path "/usr/local/go/bin")
(add-to-list 'exec-path "~/.local/bin")
(add-to-list 'exec-path "~/go/bin")
;;; (add-to-list 'exec-path "/usr/local/go/bin")
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/go/bin"))

;; ACUtex with tools.
;; Config taken from: https://nasseralkmim.github.io/notes/2016/08/21/my-latex-environment/
;; Also check pdf-tools githubpage: https://github.com/politza/pdf-tools
;; I did a (M-x pdf-tools-install) as well.
(use-package tex-site
  :ensure auctex
  :mode ("\\.tex\\'" . latex-mode)
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (rainbow-delimiters-mode)
              (company-mode)
              (smartparens-mode)
              (turn-on-reftex)
              (setq reftex-plug-into-AUCTeX t)
              (reftex-isearch-minor-mode)
              (setq TeX-PDF-mode t)
              (setq TeX-source-correlate-method 'synctex)
              (setq TeX-source-correlate-start-server t)))

;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
           #'TeX-revert-document-buffer)

;; to use pdfview with auctex
(add-hook 'LaTeX-mode-hook 'pdf-tools-install)

;; to use pdfview with auctex
(setq TeX-view-program-selection '((output-pdf "pdf-tools"))
       TeX-source-correlate-start-server t)
(setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view"))))

(use-package reftex
  :defer t
  :config
  (setq reftex-cite-prompt-optional-args t)); Prompt for empty optional arguments in cite

(use-package ivy-bibtex
  :bind ("C-c C-b b" . ivy-bibtex)
  :config
  (setq bibtex-completion-bibliography
        '("C:/Users/Nasser/OneDrive/Bibliography/references-zot.bib"))
  (setq bibtex-completion-library-path
        '("C:/Users/Nasser/OneDrive/Bibliography/references-pdf"
          "C:/Users/Nasser/OneDrive/Bibliography/references-etc"))

  ;; using bibtex path reference to pdf file
  (setq bibtex-completion-pdf-field "File")

  ;;open pdf with external viwer foxit
  (setq bibtex-completion-pdf-open-function
        (lambda (fpath)
          (call-process "C:\\Program Files (x86)\\Foxit Software\\Foxit Reader\\FoxitReader.exe" nil 0 nil fpath)))

  (setq ivy-bibtex-default-action 'bibtex-completion-insert-citation))

(use-package pdf-tools
  :mode ("\\.pdf\\'" . pdf-tools-install)
  :bind ("C-c C-g" . pdf-sync-forward-search)
  :defer t
  :config
  (setq mouse-wheel-follow-mouse t)
  (setq pdf-view-resize-factor 1.10))

;;; Docker related
(use-package dockerfile-mode
  :demand t)

(use-package docker-compose-mode
  :demand t)

(use-package docker-tramp
  :demand t)

;;; End Docker related

;;; Emacs for presentations
(use-package epresent
  :ensure t)

;;; Markdown mode
(use-package markdown-mode
;;  :ensure-system-package pandoc
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
     ("\\.md\\'" . markdown-mode)
     ("\\.markdown\\'" . markdown-mode))
  :config
  (setf markdown-command "pandoc")

  (setf next-error-highlight t)
  (bind-key "n" 'next-error ivy-occur-grep-mode-map)
  (setf next-error-highlight t)

  ;; (setf next-error-recenter '(4))
  (setf next-error-recenter nil))

;;; grip-mode github-flavored markdown preview mode.
;;; https://github.com/seagle0128/grip-mode
(use-package grip-mode
  :ensure t
  :bind (:map markdown-mode-command-map
			  ("g" . grip-mode))
;;; webkit seems broken currently so run grip-mode in external browser instead.
  :config
;;  (setq grip-github-user "tse77")
  (setq grip-github-password "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")
  ;; When nil, update the preview after file saves only, instead of also
  ;; after every text change
  (setq grip-update-after-change nil))
  ;; (setq grip-preview-use-webkit t)
  ;; (setq grip-url-browser "webkit"))

;;; webkit browser
;;; https://github.com/akirakyle/emacs-webkit
(add-to-list 'load-path "~/repos/emacs-related/emacs-webkit")
;; (use-package webkit)
;; ;;  :bind ("s-b" 'webkit))
;; (use-package 'webkit-ace) ;; If you want link hinting
;; (use-package 'webkit-dark) ;; If you want to use the simple dark mode

;; Force webkit to always open a new session instead of reusing a current one
(setq webkit-browse-url-force-new t)

(modify-frame-parameters nil '((inhibit-double-buffering . t)))
(require 'webkit)
(global-set-key (kbd "s-b") 'webkit) ;; Bind to whatever global key binding you want if you want
(require 'webkit-ace) ;; If you want link hinting
(require 'webkit-dark) ;; If you want to use the simple dark mode
;; Set webkit as the default browse-url browser
;; (setq browse-url-browser-function 'webkit-browse-url)
;; (setq browse-url-generic-program 'webkit-browse-url)
;;; END WEBKIT

(use-package flyspell
  :ensure t
  :defer t
  :init
  (progn
    (add-hook 'prog-mode-hook 'flyspell-prog-mode)
    (add-hook 'text-mode-hook 'flyspell-mode)
    )
  :config
  ;; Sets flyspell correction to use two-finger mouse click
  (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
  )


;; web, cloud related
(use-package request
  :ensure t
  :defer t)

(use-package graphql-mode
  :ensure t
  :defer t)

;;; Example section
;;; How to implements a function in elisp
(defun test ()
  (interactive)
  (mark-whole-buffer)
  (delete-trailing-whitespace))

(bind-key "C-c d" 'test)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("6dc02f2784b4a49dd5a0e0fd9910ffd28bb03cfebb127a64f9c575d8e3651440" "d0aa1464d7e55d18ca1e0381627fac40229b9a24bca2a3c1db8446482ce8185e" "732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "1a6d627434899f6d21e35b85fee62079db55ef04ecd9b70b82e5d475406d9c69" "4e132458143b6bab453e812f03208075189deca7ad5954a4abb27d5afce10a9a" "e95ad48fd7cb77322e89fa7df2e66282ade015866b0c675b1d5b9e6ed88649b4" "b0fd04a1b4b614840073a82a53e88fe2abc3d731462d6fde4e541807825af342" "155a5de9192c2f6d53efcc9c554892a0d87d87f99ad8cc14b330f4f4be204445" "4ea0aa360264ff861fb0212abe4161b83ad1d8c8b74d8a04bcd1baf0ebdceeae" "8e04ea7bf8a736b0bfacd363f4810ffce774ff9ba24f356172ae2b83307aebb2" "1d265677f99a321b640e4d762fba7c5e32ebb75250999ba53577e2a9eb009a3e" "ef4edbfc3ec509612f3cf82476beddd2aeb3da7bdc3a35726337a0cc838a4ef4" "427fa665823299f8258d8e27c80a1481edbb8f5463a6fb2665261e9076626710" "db10381a554231a40b7474eaac28bd58f05067faacce3b25d294bb179a3511a1" "868abc288f3afe212a70d24de2e156180e97c67ca2e86ba0f2bf9a18c9672f07" "cb477d192ee6456dc2eb5ca5a0b7bd16bdb26514be8f8512b937291317c7b166" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "ab9456aaeab81ba46a815c00930345ada223e1e7c7ab839659b382b52437b9ea" "34c99997eaa73d64b1aaa95caca9f0d64229871c200c5254526d0062f8074693" "8c847a5675ece40017de93045a28ebd9ede7b843469c5dec78988717f943952a" "e3c87e869f94af65d358aa279945a3daf46f8185f1a5756ca1c90759024593dd" "43c808b039893c885bdeec885b4f7572141bd9392da7f0bd8d8346e02b2ec8da" "8aca557e9a17174d8f847fb02870cb2bb67f3b6e808e46c0e54a44e3e18e1020" "75d3dde259ce79660bac8e9e237b55674b910b470f313cdf4b019230d01a982a" "d1b4990bd599f5e2186c3f75769a2c5334063e9e541e37514942c27975700370" "4697a2d4afca3f5ed4fdf5f715e36a6cac5c6154e105f3596b44a4874ae52c45" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "a3fa4abaf08cc169b61dea8f6df1bbe4123ec1d2afeb01c17e11fdc31fc66379" "fd944f09d4d0c4d4a3c82bd7b3360f17e3ada8adf29f28199d09308ba01cc092" "d2e9c7e31e574bf38f4b0fb927aaff20c1e5f92f72001102758005e53d77b8c9" "100e7c5956d7bb3fd0eebff57fde6de8f3b9fafa056a2519f169f85199cc1c96" "fe666e5ac37c2dfcf80074e88b9252c71a22b6f5d2f566df9a7aa4f9bea55ef8" "6d589ac0e52375d311afaa745205abb6ccb3b21f6ba037104d71111e7e76a3fc" "cd736a63aa586be066d5a1f0e51179239fe70e16a9f18991f6f5d99732cabb32" "10461a3c8ca61c52dfbbdedd974319b7f7fd720b091996481c8fb1dded6c6116" "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36" default))
 '(dap-mode t)
 '(dap-print-io t)
 '(grip-preview-use-webkit nil)
 '(lsp-log-io nil)
 '(lsp-pyls-plugins-pylint-enabled t)
 '(lsp-pyls-server-command '("pyls"))
 '(lsp-response-timeout 60)
 '(lsp-treemacs-sync-mode t)
 '(org-roam-mode t nil (org-roam))
 '(package-selected-packages
   '(emacsql-sqlite3 pylint company-box modus-themes lsp-docker company-capf webkit grip-mode request graphql-mode wgrep tron-legacy-theme org-bullets org-roam kubernetes ivy-rich auctex org-brain dap-go dap-mode markdown-mode+ emacs-material-theme material-theme flycheck-golangci-lint epresent lsp-ivy move-text treemacs-projectile treemacs-magit treemacs-icons-dired lsp-treemacs fontawesome blacken py-yapf py-isort dumb-jump docker-tramp docker-compose-mode dockerfile-mode pdf-tools ivy-bibtex deft doom-modeline doom-themes all-the-icons company-lsp lsp-ui lsp-mode go-snippets exec-path-from-shell crux xclip flycheck company-tern tern react-snippets company-web web-mode protobuf-mode yaml-mode company zygospore ace-window multiple-cursors counsel-projectile counsel ivy magit yasnippet-snippets yasnippet eyebrowse which-key ibuffer-vc direnv ibuffer-projectile go-projectile projectile use-package))
 '(warning-suppress-log-types '((lsp-mode))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)
